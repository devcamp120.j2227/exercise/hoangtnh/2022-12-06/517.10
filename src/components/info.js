const { Component } = require("react");

class Info extends Component{
    render(){
        console.log(this.props);
        const {firstName, lastName, favNumber} = this.props;
        return (
            <div>
                My name is {lastName} {firstName} and my favorite number is {favNumber}.
            </div>
        )
    }
}
export default Info;